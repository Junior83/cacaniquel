package njdevelopment;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class JogoCacaNiquel {

	private JFrame frame;
	private JLabel lblResultadoMoeda, lblResultadoPontuacao;
	private JButton btn1, btn2, btn3, btnApostar;
	
	int moedas, pontos;
	
	public void checarJogo(int a, int b, int c){
		
		if(a == 7 && b == 7 && c == 7){
			
			moedas += 100;
			pontos += 100;
			JOptionPane.showMessageDialog(null, "Parabéns, você é o Vencedor!");
			lblResultadoMoeda.setText(String.valueOf(moedas));
			lblResultadoPontuacao.setText(String.valueOf(pontos));
			btn1.setEnabled(false);
			btn2.setEnabled(false);
			btn3.setEnabled(false);
			
		}else{
			if(a == 7 && b ==7 || a == 7 && c ==7 || b == 7 && c == 7){
				
				moedas += 5;
				pontos += 5;
				lblResultadoMoeda.setText(String.valueOf(moedas));
				lblResultadoPontuacao.setText(String.valueOf(pontos));
				
			}else{
				if(a == 7 || b == 7 || c == 7){
					
					moedas += 1;
					pontos += 1;
					lblResultadoMoeda.setText(String.valueOf(moedas));
					lblResultadoPontuacao.setText(String.valueOf(pontos));
					
				}else{
					if(a != 7 && b !=7 && c != 7){
						
						moedas -= 1;
						lblResultadoMoeda.setText(String.valueOf(moedas));
						lblResultadoPontuacao.setText(String.valueOf(pontos));
						
						if(moedas == 0){
							JOptionPane.showMessageDialog(null, "Você Perdeu!");
							btn1.setEnabled(false);
							btn2.setEnabled(false);
							btn3.setEnabled(false);
							btnApostar.setEnabled(false);
						}
					}
				}
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JogoCacaNiquel window = new JogoCacaNiquel();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JogoCacaNiquel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(240, 248, 255));
		frame.setResizable(false);
		frame.setBounds(100, 100, 624, 485);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Caça-Niquel by NJdevelopment");
		
		btn1 = new JButton("");
		btn1.setBackground(Color.WHITE);
		btn1.setForeground(Color.RED);
		btn1.setFont(new Font("Zapfino", Font.BOLD | Font.ITALIC, 35));
		btn1.setEnabled(false);
		btn1.setBounds(6, 6, 196, 148);
		frame.getContentPane().add(btn1);
		
		btn2 = new JButton("");
		btn2.setBackground(Color.WHITE);
		btn2.setForeground(Color.RED);
		btn2.setFont(new Font("Zapfino", Font.BOLD | Font.ITALIC, 35));
		btn2.setEnabled(false);
		btn2.setBounds(214, 6, 196, 148);
		frame.getContentPane().add(btn2);
		
		btn3 = new JButton("");
		btn3.setBackground(Color.WHITE);
		btn3.setForeground(Color.RED);
		btn3.setFont(new Font("Zapfino", Font.BOLD | Font.ITALIC, 35));
		btn3.setEnabled(false);
		btn3.setBounds(422, 6, 196, 148);
		frame.getContentPane().add(btn3);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Placar Geral", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(43, 29, 222)));
		panel.setBounds(6, 166, 612, 185);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		panel.setFont(new Font("Lucida Bright", Font.BOLD, 30)); // Mudar fonte do Painel
		
		JLabel lblTituloMoeda = new JLabel("Quantidade de Moedas:");
		lblTituloMoeda.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblTituloMoeda.setBounds(20, 43, 309, 41);
		panel.add(lblTituloMoeda);
		
		JLabel lblTituloPontuacao = new JLabel("Pontuação............................:");
		lblTituloPontuacao.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblTituloPontuacao.setBounds(20, 96, 309, 41);
		panel.add(lblTituloPontuacao);
		
		lblResultadoMoeda = new JLabel("");
		lblResultadoMoeda.setForeground(Color.ORANGE);
		lblResultadoMoeda.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblResultadoMoeda.setBounds(341, 43, 265, 41);
		panel.add(lblResultadoMoeda);
		
		lblResultadoPontuacao = new JLabel("");
		lblResultadoPontuacao.setForeground(Color.ORANGE);
		lblResultadoPontuacao.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblResultadoPontuacao.setBounds(341, 96, 265, 41);
		panel.add(lblResultadoPontuacao);
		
		btnApostar = new JButton("Apostar");
		btnApostar.setForeground(new Color(0, 128, 128));
		btnApostar.setEnabled(false);
		btnApostar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// BOTÃO APOSTAR
				
				int i1, i2, i3;
				
				Random r1 = new Random();
				Random r2 = new Random();
				Random r3 = new Random();
				
				i1 = r1.nextInt(10);
				i2 = r2.nextInt(10);
				i3 = r3.nextInt(10);
				
				btn1.setText(String.valueOf(i1));
				btn2.setText(String.valueOf(i2));
				btn3.setText(String.valueOf(i3));
				
				btn1.setEnabled(true);
				btn2.setEnabled(true);
				btn3.setEnabled(true);
				
				checarJogo(i1, i2, i3);
				//======================================
			}
		});
		btnApostar.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		btnApostar.setBounds(6, 363, 196, 94);
		frame.getContentPane().add(btnApostar);
		
		JButton btnNovoJogo = new JButton("Novo Jogo");
		btnNovoJogo.setForeground(new Color(0, 0, 205));
		btnNovoJogo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// NOVO JOGO
				
				moedas = 20;
				pontos = 0;
				btn1.setEnabled(true);
				btn2.setEnabled(true);
				btn3.setEnabled(true);
				btnApostar.setEnabled(true);
				lblResultadoMoeda.setText(String.valueOf(moedas));
				lblResultadoPontuacao.setText(String.valueOf(pontos));
				//======================================
			}
		});
		btnNovoJogo.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		btnNovoJogo.setBounds(214, 363, 196, 94);
		frame.getContentPane().add(btnNovoJogo);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setForeground(Color.RED);
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// BOTÃO SAIR
				
				System.exit(0);
				//======================================
			}
		});
		btnSair.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		btnSair.setBounds(422, 363, 196, 94);
		frame.getContentPane().add(btnSair);
	}
}
